
import {Dimensions} from 'react-native';

const {width, height} = Dimensions.get('window');
// ​const width=450;
// const height=500;
//Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

const scale = size => (width / guidelineBaseWidth) * size;
const verticalScale = size => (height / guidelineBaseHeight) * size;
const moderateScale = (size, factor = 0.1) =>
  size + (scale(size) - size) * factor;

export {scale, verticalScale, moderateScale};