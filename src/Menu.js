import React, {Component} from 'react';
import {
    View, ImageBackground,
    Text, TouchableOpacity, Modal
} from 'react-native';
import {moderateScale, verticalScale} from "./sizing";

export default class Menu extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ImageBackground
                style={{width: '100%', height: '100%'}}
                source={require('../assets/images/background.jpg')}
            >
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <TouchableOpacity style={{
                        width: '80%',
                        height: verticalScale(60),
                        borderWidth: 2,
                        borderRadius: 10,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                                      onPress={() => {
                                          this.props.navigation.navigate('CreateGood',
                                              {
                                                  headers: this.props.route.params.headers
                                              }
                                          );
                                      }}>
                        <Text style={{fontSize: moderateScale(24), fontFamily: 'YekanBakh-Bold'}}>
                            {'ایجاد محصول'}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        marginTop: verticalScale(30),
                        width: '80%',
                        height: verticalScale(60),
                        borderWidth: 2,
                        borderRadius: 10,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                                      onPress={() => {
                                          this.props.navigation.navigate('goodsList',
                                              {
                                                  headers: this.props.route.params.headers
                                              }
                                          );
                                      }}>
                        <Text style={{fontSize: moderateScale(24), fontFamily: 'YekanBakh-Bold'}}>
                            {'لیست محصولات ایجاد شده'}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        marginTop: verticalScale(30),
                        width: '80%',
                        height: verticalScale(60),
                        borderWidth: 2,
                        borderRadius: 10,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}
                                      onPress={() => {
                                          this.props.navigation.navigate('ShowGood',
                                              {
                                                  headers: this.props.route.params.headers
                                              }
                                          );
                                      }}>
                        <Text style={{fontSize: moderateScale(24), fontFamily: 'YekanBakh-Bold'}}>
                            {'نمایش تکی محصول'}
                        </Text>
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        )
    }
}
