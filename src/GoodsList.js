import React, {Component} from 'react';
import {
    View, Image,
    Text, FlatList,
    TouchableOpacity, ImageBackground,
} from 'react-native';
import axios from "axios";
import {moderateScale, verticalScale} from "./sizing";

let dataArray = []
let data = {}
export default class GoodsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            flatlistData: []
        }
        axios.get('https://bazarti.com/api/user/product', {
            headers: this.props.route.params.headers
        })
            .then(response => {
                if (response.status === 200) {
                    for (let dataItem of response.data.data) {
                        data = {
                            name: dataItem.name,
                            description: dataItem.description,
                            image: dataItem.image,
                            min_price: dataItem.min_price,
                            created_at: dataItem.created_at.split(' ')[0],
                            status_text: dataItem.status_text,
                        }
                        dataArray.push(data)
                    }
                    this.setState({
                        flatlistData: dataArray
                    })

                }
            })
            .catch(error => {
                console.log(error);
            });
    }

    render() {
        return (
            <ImageBackground
                style={{width: '100%', height: '100%'}}
                source={require('../assets/images/background.jpg')}
            >
                <View style={{flex: 1}}>
                    <View style={{
                        width: '100%',
                        height: verticalScale(90),
                        alignItems: 'center',
                        flexDirection: 'row'
                    }}>
                        <TouchableOpacity
                            style={{marginLeft: '6%', width: moderateScale(18), height: moderateScale(18)}}
                            onPress={() => {
                                this.props.navigation.navigate('Menu',
                                    {
                                        headers: this.props.route.params.headers
                                    }
                                );
                            }}
                        >
                            <Image
                                style={{width: moderateScale(18), height: moderateScale(18)}}
                                source={require('../assets/images/leftArrow.png')}
                            />
                        </TouchableOpacity>
                        <Text style={{fontFamily: 'XM Yekan Regular', fontSize: 25, marginLeft: '20%'}}>
                            {'لیست محصولات'}
                        </Text>
                    </View>
                    <FlatList
                        numColumns={2}
                        data={dataArray}
                        renderItem={({item}) => {
                            return (
                                <View
                                    style={{
                                        alignSelf: 'center',
                                        width: '50%',
                                        height: verticalScale(107),
                                        borderWidth: 0.7,
                                    }}>
                                    <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                                        <View style={{
                                            height: verticalScale(100),
                                            alignItems: 'center',
                                            justifyContent: 'space-around'
                                        }}>
                                            <Text style={{fontFamily: 'IranSans'}}>{item.created_at}</Text>
                                            <Text style={{fontFamily: 'IranSans'}}>{item.status_text}</Text>
                                            <Image
                                                style={{
                                                    width: moderateScale(50),
                                                    height: moderateScale(50),
                                                    borderWidth: 1
                                                }}
                                                source={{
                                                    uri: item.image
                                                }}
                                            />
                                        </View>
                                        <View style={{height: verticalScale(107), justifyContent: 'space-around'}}>
                                            <Text
                                                style={{fontFamily: 'IranSans', textAlign: 'right'}}
                                            >
                                                {item.name}
                                            </Text>
                                            <Text style={{
                                                textAlign: 'right',
                                                fontFamily: 'IranSans'
                                            }}>
                                                {item.description}
                                            </Text>
                                            <Text
                                                style={{
                                                    textAlign: 'right',
                                                    fontFamily: 'IranSans'
                                                }}>
                                                {item.min_price}
                                            </Text>
                                        </View>

                                    </View>
                                </View>

                            )
                        }}
                    />
                </View>
            </ImageBackground>
        )
    }
}
