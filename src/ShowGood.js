import React, {Component} from 'react';
import {
    View, Dimensions, ScrollView,
    Text, TouchableOpacity, Image, FlatList, ImageBackground
} from 'react-native';
import axios from "axios";
import {moderateScale, verticalScale} from "./sizing";

let similarGood = {}
let similarGoods = []
const {width, height} = Dimensions.get('window');

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            short_description: '',
            image: '',
            min_price: '',
            created_at: '',
            adaptive: '',
            status_Text: '',
            sellerName: '',
            sellerEmail: '',
            sellerCreated_at: '',
            sellerShow_user_name: '',
            profileMobile: '',
            profilePhone: '',
            profileBusiness_name: '',
            profileCreated_at: '',
            similar_products: []
        }
        axios.get('https://bazarti.com/api/product/single/1976', {
            headers: this.props.route.params.headers
        })
            .then(response => {
                if (response.status === 200) {
                    this.setState({
                        image: response.data.data.product.image,
                        Created_at: response.data.data.product.created_at.split(' ')[0],
                        short_description: response.data.data.product.short_description,
                        name: response.data.data.product.name,
                        min_price: response.data.data.product.min_price,
                        adaptive: response.data.data.product.adaptive,
                        status_Text: response.data.data.product.status_text,
                        sellerName: response.data.data.product.seller.name,
                        sellerEmail: response.data.data.product.seller.email,
                        profileBusiness_name: response.data.data.product.seller.profile.business_name,
                        sellerCreated_at: response.data.data.product.seller.created_at.split(' ')[0],
                        profilePhone: response.data.data.product.seller.profile.phone,
                        profileMobile: response.data.data.product.seller.profile.mobile
                    })
                    for (let similarItem of response.data.data.similar_products) {
                        similarGood = {
                            name: similarItem.name,
                            description: similarItem.description,
                            positive_points: similarItem.positive_points,
                            image: similarItem.image,
                            min_price: similarItem.min_price,
                            created_at: similarItem.created_at.split(' ')[0],
                            status_text: similarItem.status_text,
                        }
                        similarGoods.push(similarGood)
                    }
                    this.setState({
                        similar_products: similarGoods
                    })

                }
            })
            .catch(error => {
                console.log(error);
            });
    }

    render() {
        return (
            <ImageBackground
                style={{width: '100%', height: '100%'}}
                source={require('../assets/images/background.jpg')}
            >
                <ScrollView style={{flex: 1}}>
                    <View style={{
                        width: '100%',
                        height: verticalScale(90),
                        alignItems: 'center',
                        flexDirection: 'row'
                    }}>
                        <TouchableOpacity
                            style={{marginLeft: '6%', width: moderateScale(18), height: moderateScale(18)}}
                            onPress={() => {
                                this.props.navigation.navigate('Menu',
                                    {
                                        headers: this.props.route.params.headers
                                    }
                                );
                            }}
                        >
                            <Image
                                style={{width: moderateScale(18), height: moderateScale(18)}}
                                source={require('../assets/images/leftArrow.png')}
                            />
                        </TouchableOpacity>
                        <Text style={{fontFamily: 'XM Yekan Regular', fontSize: 25, marginLeft: '20%'}}>
                            {'نمایش تکی محصول'}
                        </Text>
                    </View>
                    <View style={{width: '100%', height: moderateScale(80), flexDirection: 'row'}}>
                        <View style={{flexDirection: 'column', marginLeft: '8%', justifyContent: 'center'}}>
                            <Text
                                style={{
                                    textAlign: 'center',
                                    fontFamily: 'XM Yekan Regular'
                                }}>

                                {this.state.Created_at}
                            </Text>
                        </View>
                        <Image
                            style={{
                                borderRadius: 10,
                                marginLeft: '15%',
                                width: moderateScale(80),
                                height: moderateScale(80),
                                borderWidth: 1,
                                alignSelf: 'center'
                            }}
                            source={{
                                uri: this.state.image
                            }}
                        />
                    </View>
                    <View style={{width: '100%', height: verticalScale(100)}}>
                        <Text style={{
                            textAlign: 'center',
                            fontFamily: 'XM Yekan Regular',
                            fontSize: moderateScale(25),
                            marginTop: verticalScale(4)
                        }}>
                            {this.state.name}
                        </Text>
                        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                            <Text style={{fontFamily: 'XM Yekan Regular', fontSize: 18}}>
                                {'قیمت :'}
                                {this.state.min_price}
                            </Text>
                            <Text style={{fontFamily: 'XM Yekan Regular', fontSize: 18}}>
                                {'شرح :'}
                                {this.state.short_description}
                            </Text>
                        </View>
                        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                            <Text style={{fontFamily: 'XM Yekan Regular', fontSize: 18}}>
                                {'نوع :'}
                                {this.state.adaptive}
                            </Text>
                            <Text style={{fontFamily: 'XM Yekan Regular', fontSize: 18}}>
                                {'وضعیت :'} {this.state.status_Text}
                            </Text>
                        </View>
                    </View>
                    <View style={{
                        width: '90%',
                        alignSelf: 'center',
                        borderWidth: 1,
                        borderRadius: 10,
                        height: verticalScale(135),
                        marginTop: verticalScale(15)
                    }}>
                        <Text style={{
                            marginRight: width * 0.03,
                            marginTop: width * 0.03,
                            fontFamily: 'XM Yekan Regular',
                            fontSize: moderateScale(19)
                        }}>
                            {'اطلاعات فروشنده :'}
                        </Text>
                        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                            <Text style={{fontFamily: 'XM Yekan Regular'}}>
                                {'ایمیل :'}{this.state.sellerEmail}
                            </Text>
                            <Text style={{fontFamily: 'XM Yekan Regular'}}>
                                {'نام :'}{this.state.sellerName}
                            </Text>
                        </View>
                        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                            <View style={{flexDirection: 'row'}}>
                                <Text>
                                    {this.state.sellerCreated_at}
                                </Text>
                                <Text style={{fontFamily: 'XM Yekan Regular'}}>
                                    {'تاریخ :'}
                                </Text>
                            </View>
                            <Text style={{fontFamily: 'XM Yekan Regular'}}>{this.state.profileBusiness_name}</Text>
                        </View>
                        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                            <Text style={{fontFamily: 'XM Yekan Regular'}}>
                                {'شماره همراه :'}
                                {this.state.profileMobile}
                            </Text>
                            <Text style={{fontFamily: 'XM Yekan Regular'}}>{'تلفن :'} {this.state.profilePhone}</Text>
                        </View>
                    </View>

                    <View>
                        <View style={{width: '100%', height: verticalScale(30), marginTop: verticalScale(7)}}>
                            <Text style={{
                                alignSelf: 'flex-end',
                                fontFamily: 'XM Yekan Regular',
                                fontSize: moderateScale(19),
                                marginRight: width * 0.062
                            }}>{'محصولات مشابه :'}</Text>
                        </View>
                        <FlatList
                            data={this.state.similar_products}
                            renderItem={({item}) => {
                                return (
                                    <View style={{
                                        borderRadius: 10,
                                        borderWidth: 1,
                                        width: width * 0.90,
                                        marginTop: verticalScale(10),
                                        marginLeft: '5%',
                                    }}>
                                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                                            <View style={{
                                                marginLeft: moderateScale(10),
                                                marginTop: verticalScale(5),
                                                width: width * 0.9 - verticalScale(105)
                                            }}>
                                                <Text style={{fontFamily: 'XM Yekan Regular'}}>{item.description}</Text>
                                                <Text style={{fontFamily: 'XM Yekan Regular'}}> مزیت
                                                    : {item.positive_points}</Text>
                                            </View>
                                            <View style={{
                                                flexDirection: 'column',
                                                width: verticalScale(80),
                                                marginRight: verticalScale(5),
                                                marginTop: verticalScale(5)
                                            }}>
                                                <Image
                                                    source={{
                                                        uri: item.image
                                                    }}
                                                    style={{
                                                        borderRadius: 10,
                                                        width: verticalScale(80),
                                                        height: verticalScale(90)
                                                    }}
                                                />
                                                <Text style={{
                                                    fontFamily: 'XM Yekan Regular',
                                                    alignSelf: 'center'
                                                }}>{item.name}</Text>
                                                <Text style={{
                                                    fontFamily: 'XM Yekan Regular',
                                                    alignSelf: 'center',
                                                    color: 'red'
                                                }}>{item.min_price}</Text>
                                                <Text style={{
                                                    fontFamily: 'XM Yekan Regular',
                                                    alignSelf: 'center',
                                                    color: 'red'
                                                }}>{item.created_at}</Text>
                                                <Text style={{
                                                    fontFamily: 'XM Yekan Regular',
                                                    alignSelf: 'center',
                                                    color: 'red'
                                                }}>{item.status_text}</Text>
                                            </View>
                                        </View>

                                    </View>
                                )
                            }}
                        />
                    </View>
                </ScrollView>
            </ImageBackground>
        )
    }
}
