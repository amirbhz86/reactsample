import React, {Component} from 'react';
import {
    View, Modal,
    Text, TextInput, TouchableOpacity, ImageBackground
} from 'react-native';
import {NavigationContainer, CommonActions} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import axios from 'axios';
import CreateGood from "./src/CreateGood";
import Menu from './src/Menu'
import GoodsList from './src/GoodsList';
import ShowGood from "./src/ShowGood";
import {moderateScale, verticalScale} from "./src/sizing";

let headers = {}
let Bearer = 'Bearer '

class LoginPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            UserEmail: '',
            UserPassword: '',
        }
    }

    userLogin = () => {
        const {UserEmail, UserPassword} = this.state;
        axios.post('https://bazarti.com/api/login', {
            email: UserEmail,
            password: UserPassword,
        })
            .then(response => {
                if (response.status === 200) {
                    headers = {
                        Accept: 'application/json',
                        Authorization: Bearer.concat(response.data.data.token)
                    }
                    this.props.navigation.navigate('Menu',
                        {
                            headers: headers
                        }
                    );
                }else{
                    alert('ارتباط برقرار نشد')
                }

            })
            .catch(error => {
                console.log('error');
            });
    }

    render() {
        return (
            <ImageBackground
                style={{width: '100%', height: '100%'}}
                source={require('./assets/images/background.jpg')}
            >
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <View style={{
                        borderRadius: 10,
                        width: '80%',
                        alignItems: 'center',
                        height: verticalScale(50),
                        borderWidth: 2
                    }}>
                        <TextInput
                            onSubmitEditing={() => {
                                this.secondTextInput.focus();
                            }}
                            blurOnSubmit={false}
                            value={this.state.UserEmail}


                            style={{
                                textAlign: 'right',
                                fontFamily: 'XM Yekan Regular',
                                width: '95%',
                                height: verticalScale(50)
                            }}
                            onChangeText={(UserEmail) => this.setState({UserEmail})}
                            placeholder={'نام کاربری'}

                        />
                    </View>
                    <View style={{
                        borderRadius: 10,
                        width: '80%',
                        alignItems: 'center',
                        height: verticalScale(50),
                        marginTop: 15,
                        borderWidth: 2
                    }}>
                        <TextInput
                            secureTextEntry={true}
                            onSubmitEditing={() => {
                                this.userLogin()
                            }}
                            ref={(input) => {
                                this.secondTextInput = input;
                            }}
                            value={this.state.UserPassword}
                            style={{
                                textAlign: 'right',
                                fontFamily: 'XM Yekan Regular',
                                width: '95%',
                                height: verticalScale(50)
                            }}
                            onChangeText={(UserPassword) => this.setState({UserPassword})}
                            placeholder={'رمز عبور'}
                        />
                    </View>
                    <TouchableOpacity
                        style={{
                            borderRadius: 5,
                            marginTop: verticalScale(25),
                            width: 80,
                            height: 40,
                            backgroundColor: '#1d8cf8',
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                        onPress={() => this.userLogin()}>
                        <Text style={{
                            fontFamily: 'XM Yekan Regular',
                            fontSize: moderateScale(20),
                            color: 'white'
                        }}>{'ورود'}</Text>
                    </TouchableOpacity>

                </View>
            </ImageBackground>
        )
    }
}


const Stack = createStackNavigator();

function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="LoginPage" headerMode="none">
                <Stack.Screen name="LoginPage" component={LoginPage}/>
                <Stack.Screen name="CreateGood" component={CreateGood}/>
                <Stack.Screen name="Menu" component={Menu}/>
                <Stack.Screen name="goodsList" component={GoodsList}/>
                <Stack.Screen name="ShowGood" component={ShowGood}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default App;





